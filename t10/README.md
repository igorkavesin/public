# Test10 develop for feature_readme
# final version release 0.0.1
# new dev version....

# new add --amend initial
# head commit get reset HEAD readme

# next test new test  7be669c
# next one... 
# git fetch origin
# git push origin master
# git tag -s ( v1.5 ) -m 'my signed 1.5 tag'

#test
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.22.

## Development server
git 
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
