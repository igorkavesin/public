
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { countNode, CountState } from './count.reducer';

export const selectCountFeature = createFeatureSelector<CountState>(countNode);

/** Overloads **/
export const selectCount = createSelector (
    selectCountFeature,
    (state: CountState): number => state.count
 );

 /** Overloads **/
export const selectUpdatedAt = createSelector (
    selectCountFeature,
    (state: CountState): number => state.updatedAt
 );

