import { CountAction, CountActionsType } from './count.actions';

export const countNode = 'count';

export interface CountState {
    count: number;
    updatedAt: number;
}

const initialState: CountState = {
    count: 0,
    updatedAt: Date.now()
};

const newState = (state, newDate) => {
    return Object.assign({}, state,  newDate);
};


export const countReducer = ( state = initialState, action: CountAction) => {

    switch(action.type) {
        case CountActionsType.more:
        // return Object.assign({},  state, { count: state.count + 1 }, { updatedAt: Date.now()  } );
        // return Object.assign({},  state, { count: state.count + 1 });

        return newState(state, {count: state.count + Math.floor(Math.random() * 1000) });
        case CountActionsType.less:

        // ... old return new {count: state.count}
        // return {
        //    ...state, count: state.count - 1, updatedAt: Date.now()
        // };

        return {
            ...state, count: state.count - 1
         };

        case CountActionsType.clear:
        return {
            ...state,
            count: 254,
            updatedAt: Date.now()
        };
        case CountActionsType.updatedAt:
            // this is contructor ( action.payload.nameKey ) // example action.payload.updatedAt
            return {
                ...state,
                updatedAt: action.payload.updatedAt
            };

        default:
            return state;
    }
};
