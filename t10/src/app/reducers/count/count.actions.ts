import { Action } from '@ngrx/store';

export enum CountActionsType {
    more = '[COUNT] more',
    less = '[COUNT] less',
    clear = '[COUNT] clear',
    updatedAt = '[COUNT] updatedAt',
}

export class CountMoreActions implements Action {
    readonly type = CountActionsType.more;
}

export class CountLessActions implements Action {
    readonly type = CountActionsType.less;
}

export class CountClearActions implements Action {
    readonly type = CountActionsType.clear;
}

export class CountUpdatedAtActions implements Action {
    readonly type = CountActionsType.updatedAt;
    constructor(public payload: {
        updatedAt: number
    } ) {}
}

export type CountAction = CountMoreActions | CountLessActions | CountClearActions | CountUpdatedAtActions;
