import { ActionReducer, ActionReducerMap,
createFeatureSelector,
createSelector, MetaReducer, Action} from '@ngrx/store';
import { CountState, countReducer, countNode } from './count/count.reducer';
import { environment } from 'src/environments/environment';

export interface State {
    // count: CountState; // interface
    [countNode]: CountState;
}


// console.log all actions
export function debug(reducer: ActionReducer<State>): ActionReducer<State> {

    return (state: State, action: any): any => {
        console.log('state===>', state);
        console.log('action====>', action);
        return reducer(state, action);
    };
    // tslint:disable-next-line: only-arrow-functions
    // return function(state, action) {
    //   console.log('state===>', state);
    //   console.log('action====>', action);
    //   return reducer(state, action);
    // };
  }

export const reducers: ActionReducerMap<State> = {
    [countNode]: countReducer // function 
    // [countNode] : countReducer <- ActionReducer is INTERFACE

};

export const metaReducers: MetaReducer<State>[] = [debug];
