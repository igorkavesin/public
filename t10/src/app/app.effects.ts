import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType} from '@ngrx/effects';
import { CountActionsType, CountUpdatedAtActions } from './reducers/count/count.actions';

import { map } from 'rxjs/operators';

@Injectable()
export class AppEffects {
    // actions$ Observable
    constructor( private actions$: Actions) { }

    @Effect()
    updatedAt$() { // updatedAt$ is Observable

       return this.actions$.pipe(
           ofType(CountActionsType.clear, CountActionsType.less, CountActionsType.more),
           map( () => {
               return new CountUpdatedAtActions({
                   updatedAt: Date.now()
               } );
           } )
       );
    }
}
