import { Component } from '@angular/core';
import { CountState, countNode } from './reducers/count/count.reducer';
import { Store, State, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, timeout } from 'rxjs/operators';
import { selectCount, selectUpdatedAt } from './reducers/count/count.selectors';
import { CountMoreActions, CountLessActions, CountClearActions } from './reducers/count/count.actions';
import { Action } from 'rxjs/internal/scheduler/Action';
import { interval } from 'rxjs';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = ' TEST IS 10 ';

  public count$: Observable<number>;
  public diasableLess: Observable<boolean>;
  public updatedAt$: Observable<number> = this.store$.pipe(select(selectUpdatedAt) );

  constructor( private store$: Store<CountState> ) {
    this.count$ = this.store$.pipe(select(selectCount));

    this.diasableLess = this.count$.pipe( map(count => count <= 0 ) );

    this.clear();
  }
  public more() {
    this.store$.dispatch(new CountMoreActions());
  }

  public less(): void {
    this.store$.dispatch(new CountLessActions());
  }

  public clear(): void {
    this.store$.dispatch(new CountClearActions());
    interval(2000).subscribe(  () => {
      this.store$.dispatch(new CountMoreActions());

      interval(1000).subscribe(() => {
        this.store$.dispatch(new CountLessActions());
      });
    });

  }


  // var that = this; // no need of this line
 
   





  // CountUpdatedAtActions

}
